from time import sleep


class RLMCEpisode:
    """Describes an episode of the motor control rl loop."""

    def __init__(self, i: int, translation: float, rotation: float, seconds: float) -> None:
        self.i = i
        self.translation = translation
        self.rotation = rotation
        self.seconds = seconds

    def __str__(self) -> str:
        return f"translation: {self.translation}, rotation: {self.rotation}, seconds: {self.seconds}"

    def perform(self, amiro_remote_client) -> None:
        """Perform the episode using the remote client to control the AMiRo."""
        
        amiro_remote_client.set_light(4)
        amiro_remote_client.set_motor(self.translation, self.rotation)
        sleep(self.seconds)
        amiro_remote_client.set_light(0)

    def evaluate(self, tracking_recording) -> float:
        """Evaluate the loss of this episode using the recorded tracking data."""
        
        return 0

class RLMC:

    def __init__(self, num_episodes) -> None:
        self.num_episodes = num_episodes
        self.episode_count = 0

    def get_next_episode(self) -> RLMCEpisode:
        self.episode_count += 1
        return RLMCEpisode(self.episode_count-1, 0, 0.5, 5)
        

"""
1. Episode vorbereiten
    - Amiro an geeignete Position fahren
    - Amiro den aktuellen State mitteilen
2. Episode durchführen
    - Amiro für 10 Sekunden fahren lassen
    - Währenddessen mittels Tracking Position aufnehmen
3. State-Update
    - Anhand von getrackter Trajektorie Kosten berechnen
    - RL-Modell updaten
4. Von vorne beginnen
"""

# def prepare_episode(amiro_remote_client, pid, tracking_client):
#     amiro_remote_client.set_pid(pid)
#     move_to_starting_position(amiro_remote_client, tracking_client)

# def run_episode(episode_description, amiro_remote_client, tracking_client):
#     episode_description.start(amiro_remote_client)
#     tracking_recording = tracking_client.record(episode_description.duration)
#     episode_description.stop(amiro_remote_client)
#     return tracking_recording

# def update_model(model, pid, episode_description, tracking_recording):
#     loss = calc_loss(episode_description, tracking_recording)
#     model.update(pid, loss)
    

# def perform_rlmc(rlmc):
    
#     rlmc.amiro_remote_client.connect()

#     for i, episode_description in enumerate(rlmc.episodes):
#         # 1.
#         prepare_episode(episode_description, rlmc.amiro_remote_client, rlmc.pid, rlmc.tracking_client)
#         # 2.
#         tracking_recording = run_episode(episode_description, rlmc.amiro_remote_client, rlmc.tracking_client)
#         # 3.
#         update_model(rlmc.model, episode_description, tracking_recording)

from amiro_remote.client import amr
from amiro_remote.amiro import *
import matplotlib.pyplot as plt
from time import sleep


def plot_response(pid_state_values):
    # retreive individual values from pid states
    target_translation, target_rotation, translation_response, rotation_response, left_widths, right_widths = ([td.values[i] for td in pid_state_values] for i in range(6))
    timestamps = [td.timestamp for td in pid_state_values]

    # convert timestamps to relative deltas
    timestamps = [(t - timestamps[0])*1e-6 for t in timestamps]

    fig, (ax1, ax2, ax3, ax4) = plt.subplots(4)
    fig.suptitle("AMiRo Motor Response")
    
    ax1.set_title("Translation Response")
    ax1.plot(timestamps, target_translation, c="b", label="target")
    ax1.plot(timestamps, translation_response, c="k", label="response")
    ax1.legend(loc="upper left")
    
    ax2.set_title("Rotation Response")
    ax2.plot(timestamps, target_rotation, c="b", label="target")
    ax2.plot(timestamps, rotation_response, c="k", label="response")
    ax2.legend(loc="upper left")

    ax3.set_title("PWM Widths")
    ax3.plot(timestamps, left_widths, c="g", label="left")
    ax3.plot(timestamps, right_widths, c="r", label="right")
    ax3.legend(loc="upper left")    

    left_p_widths, left_i_widths, left_d_widths = ([td.values[i] for td in pid_state_values] for i in (6,7,8))
    ax4.set_title("PWM Width Components Left")
    ax4.plot(timestamps, left_p_widths, c="r", label="p")
    ax4.plot(timestamps, left_i_widths, c="g", label="i")
    ax4.plot(timestamps, left_d_widths, c="b", label="d")
    ax4.legend(loc="upper left")


    import math
    total_rot = sum(rotation_response) / len(rotation_response) * timestamps[-1]
    target_timestamps = [t for t, trans, rot, in zip(timestamps, target_translation, target_rotation) if trans != 0 or rot != 0]
    duration = target_timestamps[-1] - target_timestamps[0]
    print("duration:", duration)
    print("total radians:", total_rot)
    print("total degrees:", total_rot * 180 / math.pi)
    print("radians/sec:", total_rot/duration)

    fig.show()


def monitor_response(translation, rotation, duration, gains=None):
    with amr:
        # AMiRo drives for given duration
        if gains is not None:
            amr.set_motor_gains(gains)
        amr.collect_data(topic=Topic.DMCPidState)
        amr.set_light(5)
        amr.set_motor(translation, rotation)
        sleep(duration)
        amr.set_motor(0, 0)
        amr.set_light(0)
        sleep(1)

        # collect gathered pid controller values 
        pid_state_values = amr.collect_data(topic=Topic.DMCPidState)
    
    # plot the response
    plot_response(pid_state_values)

from __future__ import annotations
from amiro_remote.amiro_basyx import AmiroBasyx
from amiro_remote.amiro import AMiRoDef
#from amiro_remote.client import AmiroRemoteClient
from ml4proflow_mods.amiro.rlmc import RLMC
from amiro_remote.can import CANReaderWriter, getdict, flatten_dict
from ml4proflow.modules import DataFlowManager, SourceModule, ExecutableModule, Module
from typing import Any, Union
from time import time, sleep
import json
import os
import pandas as pd
import traceback


class AmiroModule(SourceModule, ExecutableModule):
    """Receive AMiRo sensor data"""

    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]) -> None:
        SourceModule.__init__(self, dfm, config)
        ExecutableModule.__init__(self, dfm, config)

        self.config.setdefault("amiro_ip", "129.70.147.50")
        self.config.setdefault("amiro_port", 1234)
        self.config.setdefault("push_filter_topics", []) #list(topic_name2id.keys())
        self.config.setdefault("push_mode", "grouped")
        self.config_range['push_mode'] = ['grouped', 'individual']
        self.config.setdefault("push_frequency", 10)
        self.config.setdefault("aas_server_url", "http://pyke.techfak.uni-bielefeld.de:9000")
        self.config.setdefault("aas_amiro_id", -1)
        self.config.setdefault("amiro_non_blocking", False)
        self.config_desc["amiro_ip"] = "The ip of the AMiro"
        self.config_desc["amiro_port"] = "The port of the AMiRo"
        self.config_desc["push_filter_topics"] = "The list of topics to filter for"
        self.config_desc["push_mode"] = "Push new data individually or send all columns"
        self.config_desc["push_frequency"] = "The frequency in Hz for pushing new data when push_mode is 'grouped'"

        self.can_reader = None
        self.update()

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        return {"name": "AMiRo Module",
                "categories": ["Data Source"],
                "html-description": "Receive sensor data from an AMiRo.",
                "jupyter-gui-cls": "ml4proflow_mods.amiro.widgets.AmiroWidget",
                "jupyter-gui-override-settings-type": {
                    "amiro_ip": "string",
                    "amiro_port": "integer",
                    "push_filter_topics": "list",
                    "push_mode": "string",
                    "push_frequency": "integer"
                    }
                }

    def update_config(self, k: str, v: Any) -> None:
        super().update_config(k, v)
        self.update()

    def update(self) -> None:
        self.current_data = {topic_name: {} for topic_name in self.config["push_filter_topics"]}
        self.latest_timestamp = None
        self.latest_data = None

    def _push_all(self, dframe: pd.DataFrame) -> None:
        for channel in self.config["channels_push"]:
            self._push_data(channel, dframe)

    def set_status(self, msg):
        if hasattr(self, 'widget'):
                    self.widget.set_status(msg)

    def execute_once(self) -> None:
        try:
            # init and connect can reader
            if self.can_reader is None:
                for k in self.config["push_filter_topics"]:
                    if type(k) != int:
                        self.set_status('Status: push_filter_topics broken!')
                        return
                amiro_def = AMiRoDef()
                if self.config['aas_amiro_id']!=-1:
                    amiro_basyx = AmiroBasyx(self.config['aas_server_url'])
                    c_by_type = amiro_basyx.get_amiro_parsed_capabilities(self.config['aas_amiro_id'], False)
                else:
                    amiro_basyx = AmiroBasyx(None)
                    with open(f'{os.path.dirname(os.path.abspath(__file__))}/msg_types.json') as f:
                        c_by_type = json.load(f)
                    c_by_type[0] = c_by_type.pop("0")
                    c_by_type[1] = c_by_type.pop("1")
                amiro_basyx.load_and_add_classes(c_by_type)
                amiro_def.load_message_defs(c_by_type)
                
                topics = [amiro_def.Topic(topic_name) for topic_name in self.config["push_filter_topics"]]
                self.can_reader = CANReaderWriter((self.config["amiro_ip"], self.config["amiro_port"]), amiro_def=amiro_def, filter_topics=topics)
                self.set_status('Status: Connecting')
                self.can_reader.connect()
                self.set_status('Status: Connected')

            if self.config["push_mode"] == "individual":
                self.individual()
            elif self.config["push_mode"] == "grouped":
                self.grouped()
        except Exception as err:
            if hasattr(self, "widget"):
                print(traceback.format_exc())
            else:
                raise err

    def cleanup(self) -> None:
        self.can_reader.disconnect()
        self.can_reader = None

    def individual(self) -> None:
        # get any data update asap and push it
        self.set_status('Status: Fetching')
        topic_data = self.can_reader.get()
        data_d = getdict(topic_data.payload)
        data_flatten = {}
        flatten_dict(data_d, data_flatten, self.can_reader.amiro_def.Topic(topic_data.topic).name[8:]+"_")
        dframe = pd.DataFrame(data_flatten) #{topic_data.topic: data_flatten})
        self._push_all(dframe)
        self.set_status('Status: Pushed data')

    def grouped(self) -> None:
        self.set_status('Status: Fetching')
        if self.latest_timestamp is None:
            self.latest_timestamp = time()

        # TODO: read all buffered messages more elegantly when widget is too slow

        # push target time
        push_time = self.latest_timestamp + 1/self.config["push_frequency"]

        # start new time window if already too late (due to the widget not keeping up)
        if time() > push_time:
            push_time = time() + 1/self.config["push_frequency"]

        # update newest data until its time to push
        num_data = 0
        while time() < push_time:
            non_blocking = self.config["amiro_non_blocking"]
            topic_data = self.can_reader.get(non_blocking=non_blocking)
            if non_blocking and topic_data is None:
                continue
            num_data += 1
            data_d = getdict(topic_data.payload)
            data_flatten = {}
            flatten_dict(data_d, data_flatten, self.can_reader.amiro_def.Topic(topic_data.topic).name[8:]+"_")
            self.current_data[topic_data.topic] = data_flatten

        elapsed = time() - self.latest_timestamp
        print(f"Time since last push: {elapsed:.1f}s (est fps: {1/elapsed:.1f}) #msgs: {num_data}")

        self.latest_timestamp = push_time
        merge_data = {}
        for d in self.current_data.values():
            for k,v in d.items():
                merge_data[k] = [v]
        self.latest_data = pd.DataFrame(merge_data)# {topic_name: pd.Series(data, dtype="float64") for topic_name, data in self.current_data.items()})
        self._push_all(self.latest_data)
        self.set_status('Status: Pushed data')


class LogModule:

    def _log(self, value) -> None:
        print(value)


class RLMCModule(ExecutableModule, Module, LogModule):
    """Control RL iterations for AMiRo motor control"""

    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
        Module.__init__(self, dfm, config)

        self.config.setdefault("num_episodes", 10)
        self.config_desc["num_episodes"] = "The number of rl episodes."

        self.rlmc = None

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        return {"name": "RLMC Module",
                "categories": ["RLMC"],
                "html-description": "Apply reinforcement learning for AMiRo motor control.",
                "jupyter-gui-cls": "ml4proflow_mods.amiro.widgets.SimpleOutputWidget",
                "jupyter-gui-override-settings-type": {
                    "num_episodes": "integer"
                    }
                }

    def execute_once(self) -> None:
        # Start rl from scratch and start first iteration
        self.rlmc = RLMC(self.config["num_episodes"])
        self.run_next_episode()

    def on_new_data(self, name: str, sender: SourceModule, data: pd.DataFrame) -> None:
        # Start next rl iteration.
        self.run_next_episode()

    def run_next_episode(self):
        """Trigger the next episode."""

        df = pd.DataFrame()
        df.episode = self.rlmc.get_next_episode()
        self._log(f"Start episode: {df.episode}")
        # pass episode description to the next module
        self._push_data(self.config["channels_push"][0], df)


#amr = AmiroRemoteClient("ip", 1234)

class RLMCPrepareEpisodeModule(Module, LogModule):
    """Prepare AMiRo for the next episode."""

    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
        super().__init__(dfm, config)

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        return {"name": "RLMC Prepare Episode Module",
                "categories": ["RLMC"],
                "html-description": "Move AMiRo to a suitable starting position and prepare episode.",
                "jupyter-gui-cls": "ml4proflow_mods.amiro.widgets.SimpleOutputWidget"
                }

    def on_new_data(self, name: str, sender: SourceModule, data: pd.DataFrame) -> None:
        self._log(f"Prepare episode: {data.episode}")
        
        # signal some stuff for 2 seconds
        with amr:
            amr.set_light(1)
            sleep(2)
            amr.set_light(0)
        
        # pass episode description to the next module that runs this episode
        self._push_data(self.config["channels_push"][0], data)

class RLMCRunEpisodeModule(Module, LogModule):
    """Run an episode and track AMiRo position."""

    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
        super().__init__(dfm, config)

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        return {"name": "RLMC Run Episode Module",
                "categories": ["RLMC"],
                "html-description": "Perform and track an rl episode.",
                "jupyter-gui-cls": "ml4proflow_mods.amiro.widgets.SimpleOutputWidget"
                }

    def on_new_data(self, name: str, sender: SourceModule, data: pd.DataFrame) -> None:
        self._log(f"Perform Episode: {data.episode}")

        # perform episode
        data.episode.perform(amr)

        # pass tracking recording and episode description to evaluate this episode
        data.tracking = []
        self._push_data(self.config["channels_push"][0], data)


class RLMCUpdateStepModule(Module, LogModule):
    """Update the model using the tracking recording of the current episode."""

    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
        super().__init__(dfm, config)

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        return {"name": "RLMC Update Step Module",
                "categories": ["RLMC"],
                "html-description": "Update the model by evaluating last episode.",
                "jupyter-gui-cls": "ml4proflow_mods.amiro.widgets.SimpleOutputWidget"
                }

    def on_new_data(self, name: str, sender: SourceModule, data: pd.DataFrame) -> None:
        self._log(f"Evaluate episode: {data.episode} \nReceived Tracking: {data.tracking}")

        # calculate loss for this episode
        loss = data.episode.evaluate(data.tracking_recording)
        self._log(f"Loss: {loss}")

        # update model ...

        # push empty dataframe to start next iteration
        self._push_data(self.config["channels_push"][0], pd.DataFrame())





    

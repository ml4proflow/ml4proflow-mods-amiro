from ml4proflow_mods.amiro.modules import AmiroModule
from ml4proflow.modules import DataFlowManager
from matplotlib.ticker import ScalarFormatter
from collections import defaultdict, deque
from time import time
import matplotlib
import matplotlib_inline
matplotlib_inline.backend_inline.set_matplotlib_formats('png')
import matplotlib.pyplot as plt
import numpy as np


# plot configuration for each topic
plot_spec = {
    0: (["FLOOR_PROXIMITY_sensors_%s" % t for t in ["left_wheel", "left_front", "right_front", "right_wheel"]], (2000, 2**16), True), #"floor_proximity"
    1: ([], (0, 2**16), False), #"floor_ambient"
    2: ([], (0, 1000), False),
    3: (["PROXIMITYSENSOR_PROXIMITY_sensors_%s" % t for t in ['nnw','wnw','wsw','ssw','sse','ese','ene','nne']], (0, 2**16), False), #"ring_ambient"
    4: (["PROXIMITYSENSOR_PROXIMITY_sensors_%s" % t for t in ['nnw','wnw','wsw','ssw','sse','ese','ene','nne']], (2000, 2**16), True), #"ring_proximity"
    5: ([], (0, 2**16), False), #"ring_ambient"
    6: (["ACCELEROMETER_values_%s" % t for t in ["x","y","z"]], (-5, 5), False), #"accelerometer"
    7: (["GYROSCOPE_values_%s" % t for t in ["x","y","z"]], (-5, 5), False), #"gyroscope"
    8: (["COMPASS_values_%s" % t for t in ["x","y","z"]], (-5, 5), False), #"compass"
    9: (["MAGNETOMETER_values_%s" % t for t in ["x","y","z"]], (-5, 5), False), #"magnetometer"
    10: ([], (-5, 5), False),
    11: ([], (-5, 5), False),
    12: ([], (-5, 5), False),
}


class AmiroPlotter:

    def __init__(self, module, blitting=False, history_size=10, big_layout=True) -> None:
        self.blitting = blitting
        self.big_layout = big_layout

        # amiro module for accessing data
        self.module = module

        # history keeps track of data to plot
        self.history_size = history_size

        # figure and axes
        self.figure = None
        self.plots = None

        self.reset()

    def reset(self) -> None:
        self.history = defaultdict(lambda: deque(maxlen=self.history_size))
        self.backgrounds = None

    def generate_plots(self) -> None:
        self.plots = dict()

        colors = ["b", "g", "r", "c", "m", "y", "k", "darkorange"]
        topic_count = len(self.module.config["push_filter_topics"])

        figsize = (25,5*topic_count) if self.big_layout else (15, 1.3*topic_count)

        self.figure, axes = plt.subplots(topic_count, 2, figsize=figsize, gridspec_kw={"width_ratios": [1, 3], "hspace": 0.5})
        self.figure.set_dpi(50)
        self.figure.tight_layout()

        for topic, (ax1, ax2) in zip(self.module.config["push_filter_topics"], axes):
            captions, (low, high), log_scale = plot_spec[topic]
            count = len(captions)
            # generate two axes for current values and history
            #fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15,5), gridspec_kw={'width_ratios': [1, 3], "wspace":0.2})
            rects1 = ax1.bar(range(count), range(count), width=0.5, animated=True)
            lines2 = [ax2.plot(range(self.history_size), range(self.history_size), color=colors[i], linewidth=2, animated=True)[0] for i in range(count)]

            # style plots accordingly
            ax1.set_ylim([low, high])
            ax2.set_ylim([low, high])
            if log_scale:
                ax1.set_yscale("log")
                ax2.set_yscale("log")
                ax1.yaxis.set_major_formatter(ScalarFormatter())
                ax2.yaxis.set_major_formatter(ScalarFormatter())
                proximity_ticks = [2000, 3000, 5000, 8000, 15000, 30000, 60000]
                ax1.set_yticks(proximity_ticks)
                ax2.set_yticks(proximity_ticks)

            ax1.grid(True, which="both", linestyle="--")
            ax2.grid(True, which="both", linestyle="--")

            ax1.set_xticks(range(count), labels=[captions[i].split('_')[-1] for i in range(count)])
            for i, bar in enumerate(rects1):
                bar.set_color(colors[i])
        
            ax1.title.set_text(self.module.can_reader.amiro_def.Topic(topic))
            ax2.title.set_text(f"last {self.history_size} values")

            self.plots[topic] = (ax1, ax2, rects1, lines2)

    def update_plots(self) -> None:
        if self.figure is None:
            self.generate_plots()

        for topic, (ax1, ax2, rects1, lines2) in self.plots.items():

            captions, _, _ = plot_spec[topic]
            count = len(captions)

            # get new values
            if topic in self.module.current_data:
                values = [self.module.current_data[topic].get(cap, 0) for cap in captions]
            else:
                values = np.zeros(count)
            if len(values) == 0:
                values = np.zeros(count)
            self.history[topic].appendleft(values)

            # update bar plot
            for rect, h in zip(rects1, values):
                rect.set_height(h)

            # update history plot
            history = np.array(self.history[topic])
            history = np.pad(history, [(0, max(0, self.history_size - history.shape[0])), (0, 0)])
            for i, line in enumerate(lines2):
                line.set_ydata(history[:, i])

            if self.blitting:
                if self.backgrounds is None:
                    plt.show(block=False)
                    plt.pause(0.1)
                    self.figure.canvas.draw()
                    self.backgrounds = dict()
                    for topic, (ax1, ax2, _, _) in self.plots.items():
                        self.backgrounds[topic] = [self.figure.canvas.copy_from_bbox(ax.bbox) for ax in [ax1, ax2]]
                    for (ax, background, artists) in zip([ax1, ax2], self.backgrounds[topic], [rects1, lines2]):
                        for artist in artists:
                            ax.draw_artist(artist)
                        self.figure.canvas.blit(ax.bbox)
                else:
                    for (ax, background, artists) in zip([ax1, ax2], self.backgrounds[topic], [rects1, lines2]):
                        self.figure.canvas.restore_region(background)
                        for artist in artists:
                            ax.draw_artist(artist)
                        self.figure.canvas.blit(ax.bbox)
                    self.figure.canvas.flush_events()


def main():
    module = AmiroModule(DataFlowManager(), dict())
    amiro_plotter = AmiroPlotter(module, blitting=True, history_size=10, big_layout=False)

    amiro_plotter.generate_plots()
    plt.show(block=False)

    start_time = time()
    while time()-start_time <= 60:
        module.execute_once()
        amiro_plotter.update_plots()
        #amiro_plotter.figure.canvas.draw()
        #amiro_plotter.figure.canvas.flush_events()
    
    module.cleanup()


if __name__ == "__main__":
    main()
from __future__ import annotations
from ml4proflow.modules import BasicModule
from ml4proflow_jupyter.widgets import BasicWidget
from ml4proflow_mods.amiro.plotting import AmiroPlotter
from amiro_remote.amiro_basyx import AmiroBasyx
from amiro_remote.amiro import AMiRoDef
from ipywidgets import DOMWidget, Output, VBox, Layout, Button, Label, HBox
from IPython.display import display, clear_output
from time import time
import cProfile
import json
import os

class AmiroWidget(BasicWidget):

    def __init__(self, module: BasicModule) -> None:
        super().__init__(module)

        # config
        self.execution_duration = 30

        # amiro plotter
        self.amiro_plotter = AmiroPlotter(module, history_size=10)
        module.widget = self

    def _get_additional_boxes(self) -> list[tuple[str, DOMWidget]]:
        return [("Connection", self._get_connection_box()), ("AAS", self._get_aas_box()), ("Visualization", self._get_visualization_box())]

    def _get_aas_box(self) -> DOMWidget:
        self.aas_execution_button = Button(description="Fetch", button_style="success")
        self.aas_execution_button.on_click(self.fetch_aas)
        header_row = HBox(children=[self.aas_execution_button])

        self.aas_output = Output(layout=Layout(display="flex", width="90%")) #height="2200px
        topics_container = VBox(children=[self.aas_output], layout=Layout(display="flex", align_items="stretch")) #height="2200px"
        return VBox(children=[header_row, topics_container])

    def set_status(self, msg):
        self.connection_status.value = msg
    
    def _get_connection_box(self) -> DOMWidget:
        self.connection_button = Button(description="Reconnect", button_style="success")
        self.connection_button.on_click(self.reconnect)
        self.connection_status = Label(value="Status: Unconnected")
        header_row = HBox(children=[self.connection_button, self.connection_status])

        return VBox(children=[header_row])

    def reconnect(self, *args, **kwargs):
        try:
            self.connection_status.value="Status: Disconnecting..."
            self.module.can_reader.disconnect()
        except:
            pass
        self.connection_status.value="Status: Unconnected"
        self.module.can_reader = None
        self.ui_stop()
    
    def fetch_aas(self, *args, **kwargs):
        amiro_def = AMiRoDef()
        if self.module.config['aas_amiro_id']!=-1:
            amiro_basyx = AmiroBasyx(self.module.config['aas_server_url'])
            c_by_type = amiro_basyx.get_amiro_parsed_capabilities(self.module.config['aas_amiro_id'], False)
        else:
            amiro_basyx = AmiroBasyx(None)
            with open(f'{os.path.dirname(os.path.abspath(__file__))}/msg_types.json') as f:
                c_by_type = json.load(f)
            c_by_type[0] = c_by_type.pop("0")
            c_by_type[1] = c_by_type.pop("1")
        amiro_def.load_message_defs(c_by_type)
        self.module.config["push_filter_topics"] = [int(k["id"]) for k in c_by_type[0] if "support_can" in k and k["support_can"]]
        self._on_module_settings_box_load(None)
        
        with self.aas_output:
            clear_output(wait=True)
            amiro_basyx.pretty_print_amiro_caps(c_by_type)

    def _get_visualization_box(self) -> DOMWidget:
        self.execution_button = Button(description="Start", button_style="success")
        self.execution_button.on_click(self.toggle_execution)
        self.execution_label = Label(f"0/{self.execution_duration}s", layout=Layout(display="flex", justify_content="center", width="60px"))
        header_row = HBox(children=[self.execution_button, self.execution_label])

        self.output = Output(layout=Layout(display="flex", width="1000px", height="2200px"))
        topics_container = VBox(children=[self.output], layout=Layout(display="flex", align_items="stretch", height="2200px"))
        return VBox(children=[header_row, topics_container])

    def update_visualization_box(self) -> None:
        # update and redraw plots
        self.amiro_plotter.update_plots()
        with self.output:
            clear_output(wait=True)
            display(self.amiro_plotter.figure)
        # update elapsed time
        self.execution_label.value = f"{int(time()-self.start_time)}/{self.execution_duration}s"

    def ui_start(self) -> None:
        self.execution_button.disabled = True
        
    def ui_stop(self) -> None:
        self.execution_button.description = "Start"
        self.execution_button.button_style = "success"
        self.execution_button.disabled = False

    def execution_loop(self) -> None:
        self.ui_start()
        self.start_time = time()
        while time() - self.start_time < self.execution_duration:
            self.module.execute_once()
            self.update_visualization_box()
        self.module.cleanup()
        self.ui_stop()

    def toggle_execution(self, *args, **kwargs) -> None:
        if len(self.module.config["push_filter_topics"])==0:
            self.set_status("Status: Visualization needs at least one push_filter_topic")
            return
        self.execution_loop()
        self.amiro_plotter.reset()


class SimpleOutputWidget(BasicWidget):
    
    def __init__(self, module: BasicModule):
        super().__init__(module)

        # capture module log in output widget
        module._log = self.output.append_display_data

    def _get_additional_boxes(self) -> list[tuple[str, DOMWidget]]:
        return [("Output", self._get_output_box())]

    def _get_output_box(self)-> DOMWidget:
        self.output = Output()
        return VBox(children=[self.output])

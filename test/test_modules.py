import unittest
from ml4proflow import modules
from ml4proflow_mods.amiro.modules import AmiroModule

class TestModulesModule(unittest.TestCase):
    def setUp(self):
        self.dfm = modules.DataFlowManager()


    def test_create_AmiroModule(self):
        dut = AmiroModule(self.dfm, {})
        self.assertIsInstance(dut, AmiroModule)


    # define several tests


if __name__ == '__main__':
    unittest.main()

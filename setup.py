from setuptools import setup, find_namespace_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

name = "ml4proflow-mods-amiro"
version = "0.0.1"

cmdclass = {}

try:
    from sphinx.setup_command import BuildDoc
    cmdclass['build_sphinx'] = BuildDoc
except ImportError:
    print('WARNING: Sphinx not available, not building docs')

setup(
    name=name,
    version=version,
    author="Christopher Niemann",
    author_email="cniemann@techfak.de",
    description="Connect an AMiRo with ml4proflow",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-amiro",
    project_urls={
        "Main framework": "https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow",
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        'Intended Audience :: Developers',
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],

    packages=find_namespace_packages(where="src"),
    package_dir={"": "src"},
    namespace_packages=['ml4proflow_mods'],
    package_data={"ml4proflow": ["py.typed"]},
    entry_points={
    },
    cmdclass=cmdclass,
    python_requires=">=3.7",
    install_requires=[
        "ml4proflow",
        "amiro-remote", # @ git+https://gitlab.ub.uni-bielefeld.de/AMiRo/amiro-esp.git@main#egg=amiro-remote&subdirectory=clients/amiro-remote
        "pandas",
        "numpy",
        "matplotlib",
    ],
    extras_require={
        "tests": ["pytest",
                  "pytest-html",
                  "pytest-cov",
                  "flake8",
                  "mypy",
                  "nbmake",
                  "types-PyYAML",
                  "jinja2==3.0.3",
                  ],
        "docs": ["sphinx", "sphinx-rtd-theme", "recommonmark"],
    },
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', version),
            'source_dir': ('setup.py', 'docs/source/'),
            'build_dir': ('setup.py', 'docs/build/')
        }
    },
)

# ml4proflow-mods-amiro

This package provides modules to connect an [AMiRo](https://doi.org/10.1109/ICSTCC.2016.7790746) with ml4proflow. 
The AMiRo can be used as data source to evaluate sensor measurements or as data sink to steer the robot.

[![Tests Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-amiro/-/jobs/artifacts/master/raw/tests-badge.svg?job=gen-cov)](#CI)
[![Coverage Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-amiro/-/jobs/artifacts/master/raw/coverage-badge.svg?job=gen-cov)](#CI)
[![Flake8 Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-amiro/-/jobs/artifacts/master/raw/flake8-badge.svg?job=gen-cov)](#CI)
[![mypy errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-amiro/-/jobs/artifacts/master/raw/mypy.svg?job=gen-cov)](#CI)
[![mypy strict errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-amiro/-/jobs/artifacts/master/raw/mypy_strict.svg?job=gen-cov)](#CI)
------------
## Installation
Activate your virtual environment (optionally) and
install the package with pip:
```bash 
pip install ml4proflow-mods-amiro
```

## Contribution
For development, install this repository in editable mode with pip:
```bash 
git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow-mods-amiro.git
cd ml4proflow-mods-amiro
pip install -e .
```

